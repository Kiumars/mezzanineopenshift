#############################
# PROJECT OVERRIDE SETTINGS #
#############################

#
# settings.py are the generic Mezzanine projects settings.
# local_settings.py are the environment settings crafted to fit both Openshift 
# environment and local development.
# You should not need to change anything in those two. 
# Best practice is to inspire form the main settings.py file and use the 
# project_override_settings.py for project related adjustements.
# This practice assures you can safely and carbon upgrade settings related to 
# the next upgrade of Mezzanine.
# 

# Search box adjustement
# http://mezzanine.jupo.org/docs/search-engine.html
SEARCH_MODEL_CHOICES = None

